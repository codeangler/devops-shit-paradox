# A DevOps Primer

Maybe not what you think?

[[_TOC_]]

## Setting the Destination

tl;dr; Showing love for others by quickly and often removing the suffering of patients and clinicians serving said patients is significant work the IT organization does. Anything that hinders the removal of suffering should be addressed. Continued improvement requires ongoing growth by moving from how work was thought best to be done in the past to how the latest research demonstrates is most effective for achieving the primary goal (i.e. loving others). The daily workplace practices captured in both the MegaCorp Values are echoed by the DevOps movement.

DevOps concept is not held by a team (despite recruiter or conference marketing have done to co-opt the term). DevOps does not equal Cloud Native (i.e. at MegaCorp working in Google Cloud Provider), however, they can be strongly complementary.

## Why? Some meditations to inspire hope

*Serving others—our reason for existing.*

- How can you make meaningful impacts on people’s lives through your work with IT?

*We never stand still; we are never satisfied.*

- How would we as an IT organization need to grow to provide such high levels of caring support to each other and others?

*One for All, and All for One! We work together, sharing a common purpose, a common culture, and common goals.*

- How can you make our service for others win-win more often and consistently?

*We take responsibility for meeting our commitments—our personal ones as well as those of the entire organization. We take ownership of the results.*

- Imagine how you will celebrate when you are able to contribute to the well-being of others, the success of overcoming challenges, and bettering your teammates’ lives all while having fun?

TLDR: the patterns and behaviors that built the company and brought us here need to continue to grow from the values of the company. Adapting and changing to care for others, be significant, predicatively provide value.

*If you need any more KoolAid let me know and I’ll place an order for you.*

## Where are we going?

 The singular measure of how IT can continuously learn to help others more often can be summed up in one behavior. Ten plus years of research has repeatedly shown that the most successful software organizations achieve their goals when they are able to achieve **Code commits merged into the master ([trunkline](https://trunkbaseddevelopment.com/)) branch put into production environments in less than 2 hours**. Everything and everyone is organized and in service of removing all constraints to achieving that singular objective. The adoption of the behaviors and focus is done while serving the company’s priorities and initiatives.

You can imagine a large warehouse building that has one side covered by a beautiful mural added to over years or decades. What you see is made of various pieces and attached with adhesives that were the best available tools and ideas at the time. As the mural spreads across the massive expanse of the warehouse wall the revealed image has a strange chaotic order, not unlike a _hidden picture_ so popular in the mid 1990s. You can remember or guess at all the smart people making the most of what was known at the time. Yet something feels off as you gaze at the mural. Why would we want to hide the picture? To make the hidden and highly valuable picture in the mosaic really pop will take revising this colossal work. You won't be able to do it alone or all at once, rather through simple, persistence and continuous prioritization to remove and replace each little piece. You know intuitively how your work and others efforts will sometimes replace bits in chunks and whole areas, while at other times it will be chip by chip. You are glad that there are examples of great masterpieces, even bigger and more complex works that have already proven a better way to make the valuable picture visible. You and your peers hopeful and are understanding that the hidden picture is hidden by a history of unintended consequences.

*Jon  note:* The above paragraph is really deep.  I had trouble resonating with the analogy.  i.e.: Our society, which loves history, cringes at the idea of taking artwork and painting over it with newer approaches to art.  Maybe the analogy of a city or a small house that has grown over time and needs to continue changing to meet the needs of modern families and residents.  I have a hard time envisioning why anyone would want to repaint parts of a mural, unless you are thinking of the painstaking work of wiping away the years of grime on the painting of the Last Supper or the ceiling of the Sistine Chapel.  But that feels different than what we are trying to do.

You are eager to learn the best way to add value and serve others by making the picture be as easy and natural to see. How can you and teammates get started? With the aid of science, leadership, a [growth mindset](https://www.mindsetworks.com/science/) and [grit](http://angeladuckworth.com/research/) the mosaic on the warehouse will become more like a living wall. You are eager to learn the best way to add value and serve others by making the picture be as easy an natural to see. You will work to make continuous improvements the norm. As the value picture is revealed teammates are thinking more and more about patient outcomes, compliance, and security. Patients are served better by clearing away any processes, products, or anti-patterns [*Jon:* i.e. old wooden buildings that are in the way of a highway you need to add so ambulances, firefighters, and employees can get around the city faster?] that are hiding the picture; that are choking this continuous growth. Was the past work bad, no not at all. Simply put, growth doesn’t stand still, it matures infinitely. Overtime too much of the old medicine and stinking thinking has accumulated. Detoxing from an overdose of these old medicines can provide a cleansing wash by feeding on the latest organizational and lean software delivery research is the only way to not slowly atrophy and die.

Look at the mural around you. What do you see? What do you feel? If you have a historic perspective, have we been moving IT towards a living system that is continuously improving and growing? Where is there gunk in the lines of communication, procedures, checklists, and too many manual gatekeeper that blocks patient care?

DevOps practices are the result of a cultural that values prioritizes tending the living mural. You begin to see how all work is to server the primary objective of getting patients better care. DevOps thinking wraps around all work, or put another way is the canvas on which work is done.

## Clearing the air, What DevOps is/is not

People are the most valuable asset of IT and having them do actives that humans do best (e.g. problem-solving, creative thinking, meta-analysis, improvements) is a way to exponentially multiple their skills, time, and talents in service of patients and caregivers. This means tasks that are repeatable, filled with numerous steps, and or computations will be given to items that do that best, that is the electronic computer! (yup history nerd, not the people computers of the 50s, 60s).

Because you are the more valuable asset, and you have adopted a growth mindset by choosing to work at MegaCorp, you are ready, willing, and able to see what the future holds. But first remember that you've already began the moment you joined. You already have been a part of DevOps cultural change each time you ask, is there a better process, can I make life better for others and myself?  Can I automate a task best completed by a computer? Can I make a process easier to repeat by putting it in code and taking it away from error prone checklists? Unless you are still an infant and somehow employed in the IT department, you've repeatedly learned new techniques and ideas throughout your life.  :-)

What are some common (mis)understandings about DevOps?

- DevOps is not a set of tooling, software, or a team of people
- DevOps does is independent of the technology, architecture, software design, delivery process, etc.
  - the work can be done in a cloud-native, on-premise or hybrid infrastructure
  - on the other hand, DevOps has some key allies in the effort to achieve its high-trust, high delivery team, and Cloud-Native is one of those allies
- DevOps is supported by a set of practices
  - e.g. infrastructure as code, automated compliance checking, etc
- DevOps is not continuing to work the same way as IT has been simply because it’s what we know

### A Definition & a Warning

DevOps is a set of software development practices that combines a software development mentality with other functions within the organization. DevOps puts a heavy emphasis on shared responsibilities across all teams throughout the software development lifecycle. The edges of job functions soften, as operations team members take on tasks that were traditionally more developer-focused and development team members do more ops tasks. The term DevOps is most commonly associated with development (Dev) and IT operations (Ops), but the approach can be extended to other groups as well including, but not limited to, security (DevSecOps), QA, database operations, and networking. [^1]

 The goal of DevOps includes the following [^1]:

- Increase collaboration among teams
- Decrease unnecessary gates and handoffs
- Empower development teams with the tools and authority to own their systems
- Create repeatable, predictable processes
- Share responsibilities for the application production environment

How do so many IT companies find they are traveling away from these goals?  One way is the story of the "delivery :poo: paradox".

> The well meaning organization say, "whoops we just delivered our customers a pile of 💩, I guess we need some more guardrails. So begins a series of review boards, gatekeepers, twelve qa and UAT environments. Finally they end up today still delivering 💩 but more infrequently which then _feels_ less threatening."

_Delivery :poo: Paradox_: while it has been tempting to think that putting more gateways/thresholds/reviews would create more reliable and higher quality software, research consistently shows the opposite is true. With slow feedback loops from long releases and many gatekeepers, the learning that could come is lost.

Slowing down delivery is proven to correlate with decreased quality of work (i.e. more bugs released, more downtime, etc). [^2]
[ Jon: !!!!! ]

## What will we be doing

Luckily there are over [ten years of research](https://about.gitlab.com/handbook/marketing/product-marketing/devops-metrics/) that reveals what successful engineering organizations do. The benchmarks from *Accelerate the Science of Lean Software Delivery*[^2] of highly performant IT organizations has found there are just these four:

### The Research!

#### Deployment Frequency

It’s the number of deployments in a period of time. It’s expected that high-performing teams deploy (to production)several times a day. This is highly correlated with the next metric.

#### Lead Time For Changes

It’s the time in between a commit has been made, and this code gets into production. The expectations for a high-performance team is to deploy changes in between one day and one week.

#### Mean Time To Restore (or MTTR)

Is the mean time that is needed to recover service when there has been a failure in production. For example, the time that we need to recover from a broken database or from a commit that breaks a feature. It’s expected that high-performers recover their services in less than a day.
[Jon: What does failure in production mean?  One function is not working right, or the whole system is down?  -- Would like to make the point that being fully down should be extremely rare]

#### Change Failure Rate

Is the ratio between failed changes and success changes in a service. For example, if we deploy four times in a week, and three of our deployments fail for some reason (there is a bug in the code, the pipeline is flaky), our CFR will be 75%. It’s expected that high-performers have a change failure rate lower than 15% percent.

[Jon: This sounds scary to me if you assume production failure means the entire system is down. If we release every day, and 15% of our days we put in a bug that takes prod down and each time those take a day to repair?]


### Some Key Elements Supporting DevOps: CAMS + L

Sometimes referred to as the pillars of DevOps CAMS + L are Culture, Automation, Metrics, and Sharing (CAMS). However these are like the living wall describe in the narrative above. No one element happens first. Each bit of work is wrapped in them and they are around every decision and action of a highly effective IT organization.

DevOps is all about learning how to work differently:

- Establish common targets and select common issues instead of optimizing processes within the department’s silos
- Cooperate on tasks with other departments instead of trying to complete them independently
- Work in a cross-functional manner instead of passing off the tasks, not caring about the results, before or after
- Share feedback with peers and management instead of trying to cover it up and resolve any issues with no outside help
- Continuously evaluate and improve instead of sticking to step-by-step processes, or pushing updates inconsistently
- Establish KPIs on important processes and religiously measure them to achieve continuous improvement
- Exercise openness, transparency, and respect instead of pushing the department’s agenda above all else
- Removing gatekeepers with automation is critical as there are too few people to watch paint dry

## How will we be doing it? (the Practical)

We’ll have to learn to do all things better. Remember the key metric? Commit to master arrives into production in less than 2-hours! That is the benchmark for measuring and evaluating success. To learn along the way we, like other companies that are succeeding at this, are delivering smaller units of value [^3][^4]. Research demonstrates such behavior is matched with lower outages and higher quality products. The feedback loop is tighter. With quicker feedback adaption, learning, and quality all increase (the opposite of the delivery :poo: paradox).

### Core Practices

**Warning**: Are you the weakest link? If you skipped the introduction and the why sections and are just looking for the how then you are going to be a weak link in the chain of success. Being clear with your “why’s” will motivate, familiarize, and strengthen your capacities to achieve. If you want to be a significant contributing member of the Village be sure to flex your "why" muscles before trying out the smaller, supportive, and more fragile "how" muscles.

Agreeing on a set of practices will help aid in the success of the Village. Practices are independent of technology and in some cases practices.

#### More Definitions and DevOps Allies

##### Techniques

*GitOps* : [read and see more](../GitOps.md)

*Innersourcing*: InnerSource takes the lessons learned from developing open source software and applies them to the way companies to develop software internally.

- [Introduction to Innersource/](https://resources.github.com/whitepapers/introduction-to-innersource/)
- [Innersource Commons](https://innersourcecommons.org/)

*Collaboration*:

- *Anyone can contribute*: waiting on someone to do work that you can do is so 1990.
  - e.g. If you need networking, infrastructure, QA, configuration change then you will open a Merge Request with the work proposed and start a conversation.
- Teach & Mentoring is normal across the Village
- High value on transparency
- The result is more and more earned trust.
- Decreased waiting for others
- Decreased unknowns about business or IT work.

*Infrastructure as Code (IAC)*: is the management of infrastructure (networks, virtual machines, load balancers, and connection topology) in a descriptive model

*Content Area Experts*: might be understood as the maintainers of an open-source project, but put another way the are the minimum viable gatekeeper to getting your amazing contribution into the code. They are charged with the task of making it effortless for others to contribute into their content area, providing mentoring and feedback.

##### Capabilities

*Review Environments:* are available on Merge Request as ephemeral infrastructure and data.

- Review Applications [https://about.gitlab.com/stages-devops-lifecycle/review-apps/](https://about.gitlab.com/stages-devops-lifecycle/review-apps/)
- Diagram [https://docs.gitlab.com/ee/ci/review_apps/#introduction](https://docs.gitlab.com/ee/ci/review_apps/#introduction)
- Review applications allow for much quicker lead times to production.
  - Ephemeral prod-like environments are created with a Merge Request and available for real interaction with real data + infrastructure + tests

*Continuous Integration*: todo

*Continuous Delivery*: todo

*Continuous Deployment*: todo

##### Tools

*Git*: is a free and open-source distributed version control system designed to handle everything from small to very large projects with speed and efficiency Git does not equal GitLab

- If you didn’t love Git before, this is your chance to fall in love with Git again and again
- Who loves git? Dev does! QA does! Security Does! Ops! Everyone Loves git!

*GitLab*: is a CI/CD + SVC + Planning + Monitoring tool that allows collaboration, work, release and monitoring to happen the same interface.

*GitLab Issue* (aka an Issue): This is a running conversation on any topic. Typically related to solving some problem, planning, or

*Merge Request* (aka Pull Request): short-lived (preferable less than one business day) open code for review and validation in the pipeline. It is a place to discuss the work that is waiting for approval.

*GitLab Epics*: capture and track Business initiatives are captured and set by Executives.

*Pipeline*: a generic term for CI/CD stages and jobs that run automated build, test, review, and release steps of the software life cycle.

*Teams*: a group of people working on an object. Teams DO NOT EQUAL projects, nor Program, nor a role or responsibility. A QA team, Infrastructure Team, Cloud Foundations Team, is an anti-pattern.

> Did you know, the first person to use term DevOps was trying to save characters in a twitter hashtag? Andrew Shaffer first referred to this work as “Agile Infrastructure.”

##### Work Flow Examples

A portion of the work flow:

![CI image](./media/chart-workflow-continuous-integration-delivery-deployment.png)


Bigger Picture of Workflow

<!-- TODO: ![GitLab Figure-eight]() -->

- Identify a problem to solve, a need, a pain, or unknown
  - ps. Starting with a solution is an anti-pattern! Start with a SMART problem statement. These are easier to collaborate around
- Open a GitLab issue and start a discussion
  - Ensure this work is aligned with business initiatives
  - Working on an issue should always be tied to a business initiative.
  - Business initiatives are captured in GitLab Epics and set by Executives.
- Take Assignment of the issue and work on a solution
  - cut a git branch from master
  - collaborate (pair?) with QA to ensure testing is comprehensive
  - push code to GitLab
- Open a Merge Request and solicit feedback.
- Adopt the feedback
- Code is "approved"
  - Some MR will require multiple approvers from different content area experts (e.g. security, networking, cloud-foundations, etc) depending on code impacted
- Code passes pipeline
- Code is Merged into Master (trunkline)
- Code is released
  - behind a canary (sub group gets feature before full roll out
  - and or behind a feature flag (e.g. disabling a feature for clinicians until they've completed a training on new feature)

#### Technical Practices of the Most Mature Technology Orgs

Technology practices and behaviors are the outcome of prioritizing outcome over output[^5]. The research shows more than a dozen practices that mature IT organizations do. However they do not do these all at once. In some area, we are already succeed. For example just using `git` is an identified valuable practice. We want this to expand to all areas of the IT organization.

We will focus on three areas:

1. Automation, particularly testing of intrafrustructure, application, and compliance
2. Infrastructure as code: spreading the ideals of _code is truth_ and deterministic design getting infrastructure into code adds value and speed
3. Simplified security model

## Who will be doing this work?

If you are wanting an invitation to participate in improving the way work gets done, or you’re waiting for someone other than you, please review MegaCorp's Values of Accountability and Continuous Improvement. You were invited to this when you were hired.

- In my area of influence, what is the next biggest constraint?
- Can I make my area of influence bigger?
- What the constraint there?
- How can the constraint to providing higher quality patient care through IT speeding up to meet the only metric that matters while I work on the task at hand?
- How can I surface constraints and drive their resolution in the context around me?
- Why does this? To be impactful, be recognized as a leader, to grow in skill + thoughts, etc

### Change is Easy

Every day you go through thousands if not millions of changes in space, thoughts, movements, relationships, etc. Why do you succeed at going through so many changes each day? Because you a familiar with them (i.e. cultural value for getting through your day), your brain has established automatic scripts (i.e. automation), you get quick and instant feedback (i.e. metrics and monitoring), you share openly (i.e. your brain gets information easily). *Yes I did just connect your brain function to CAMS. Culture, Automation, Metrics, and Sharing* This is all possible because the brain is amazing at adapting!

Now you may be tempted to say “I don’t like change.” Then it sounds like you’re retiring early because to enable the Village Mission we are either growing, maturing, improving (all change words!) or we are not. Maybe changing your perspective will help. Is it that you don’t like change or is that you don’t like the unfamiliar. Well, that’s simple to address start getting familiar by studying, listening in, and connecting your personal and professional mission with the company goals of caring for others. Motivation is simple to find. Look you are doing it already.

Do you want additional ways to change your perspective on how easy it is to live the DevOps practices and cultural values? Remember someone who you care for and image them getting care at a MegaCorp center. Now imagine them struggling with an application during scheduling, or registration, or billing. Imagine them getting a staff member who was just hand-banging on the keyboard over a faulty charting tool trying to hear your loved one's questions and concerns. You can fix all that. Simply step away from a small identity of what being a member of IT means and think of the Village and be a teammate at large because IT is focused on patient care.

#### Am I going to be replaced by DevOps? In other words will I lose my job?

If you choose join in and help with the transformation, doing your part to improve culture, automation, metrics and (proactively) sharing information, then unlikely. On the other hand if you choose to struggle against adding value to the company by adding more gates to software releases, siloing information, not-problem solving, refusing to automate repetitive tasks, enforcing checklist over agility, then yes you might get some personalized feedback and coaching.

## Quick Note about Cloud Native

  While a great ally to DevOps practices, Cloud Native is independent. Designing software, infrastructure, and solving security concerns for the cloud are going to be fresh from how work was done on-premise. Cloud-Native thinking are the guiding principles for solving these matters.

Many resources are available. One might start with learning more from the Cloud Native Foundation. The Cloud Native Computing Foundation (CNCF) hosts critical components of the global technology infrastructure. CNCF brings together the world’s top developers, end-users, and vendors and runs the largest open-source developer conferences. CNCF is part of the nonprofit Linux Foundation.

### Some free ebooks

- [Going Cloud Native](https://www.manning.com/books/going-cloud-native)
- [New stack- cloud-native](./media/TheNewStack_GuideToCloudNativeDevOps.pdf)
- [Orielly- cloud-native](./media/oreilly-cloud-native-using-containers-functions-data-next-gen-apps.pdf)

## References

- [^1]: [Operations Anti-patterns, DevOps Solutions](https://www.manning.com/books/operations-anti-patterns-devops-solutions) by Jeffery D. Smith
- [^2]: [Accelerate: The Science of Lean Software](https://itrevolution.com/devops-books/)
- [Four Key Metrics](https://www.thoughtworks.com/radar/techniques/four-key-metrics#:~:text=A%20key%20takeaway%20of%20both,)%2C%20and%20change%20fail%20percentage.)
- [Four Key Metrics](https://dev.to/kmruiz/four-key-metrics-and-team-performance-2p4f)
- [On Going DevOps Research](https://www.devops-research.com/research.html)
- [^3]: [Martin fowler Continous Delivery](https://youtu.be/aoMfbgF2D_4?t=642)
- [^4]: [GitLab on DevOps](https://about.gitlab.com/devops/)
- [Minimal Viable Change (MVC) ](https://about.gitlab.com/handbook/product/#the-minimal-viable-change-mvc)
- [Starting and Scaling DevOps in the Enterprise](./media/Starting-and-Scaling-DevOps-in-the-Enterprise.pdf) by Gary Gruver
- [^5]: [Mature DevOps Model](./media/dora-research-program.png)

